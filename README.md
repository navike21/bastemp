bastemp
=======

### Pero, ¿Qué es? ###

Bastemp es un grupo de códigos css3 utilizando flexbox, jquery, html5; que permite realizar sitios web de manera fácil rápida y amigable.

Solo usarás código HTML para las estructuras (dimensiones de la web en porcentajes, establecer un ancho mínimo o máximo, posiciones y alineamientos de contenidos y elementos, etc.) y para los formatos en css, tendras que utilizar Sass.

Está pensado para maquetadores web frontend, pero trabajaremos duro para que sea orientada a backend también.

### Versión ###
* 1.1.5

### Autor de la idea ###
* navike21.com

### Se utilizará ###

* Sass como preprocesador de css.
* Jquery como base para la dinamización de bastemp (Acciones, eventos, responsive, etc).

## Configuración ##

* <link rel="stylesheet" href="https://bastemp.com/css/bastemp.min.css?ver=1.1.5" />
* <script src="https://bastemp.com/js/bastemp.min.js?ver=1.1.5" charset="utf-8"></script>
